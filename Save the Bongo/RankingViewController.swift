//
//  RankingViewController.swift
//  Save the Bongo
//
//  Created by Maykon Meier on 2/22/15.
//  Copyright (c) 2015 Maykon Meier. All rights reserved.
//

import UIKit

struct Score {
    var name:String!
    var score:Int!
}

class RankingViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, RankingDelegate {
    
    var scores:[Score]?
    
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var table: UITableView!
    
    @IBOutlet var close:UIButton!
    
    @IBAction func closeAction(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        UserData.sharedInstance.delegateRanking = self
        UserData.sharedInstance.getScores()
    }
    
    override func viewWillDisappear(animated: Bool) {
        UserData.sharedInstance.delegateRanking = nil
    }
    
    func error(e:NSError?) {
        self.scores = [Score(name: Text.translate("FACEBOOK_NOT_LOGED_IN"), score: 0)]
        self.finishUpdate()
    }
    
    func setScores(scores:[Score]?) {
        self.scores?.removeAll(keepCapacity: true)
        self.scores = scores
//        println("Scores: \(self.scores)")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.close.setTitle(Text.translate("BACK_TO"), forState: .Normal)
        self.close.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        
        self.close.backgroundColor = UIColor.blackColor()
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "Ranking")!)
        
        self.activityIndicator.stopAnimating()
        self.activityIndicator.hidesWhenStopped = true
        
//        self.table.backgroundColor = UIColor.greenColor()
//        self.table.bounds = CGRectMake(0, 0, self.view.bounds.size.width - 50, self.view.bounds.size.height)
            
        self.table.backgroundColor = UIColor.clearColor()
        
        self.table.delegate = self
        self.table.dataSource = self
        
        self.table.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        self.view.addSubview(self.table)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func startUpdatingData() {
        self.activityIndicator.startAnimating()
    }
    
    func finishUpdate() {
        self.activityIndicator.stopAnimating()
        self.table.reloadData()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let scores = self.scores? {
            return scores.count
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
//        var cell:UITableViewCell = self.table.dequeueReusableCellWithIdentifier("cell") as UITableViewCell
        
        let cellIdentifier = "cell"
        
        var cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: cellIdentifier)
        
        cell.textLabel?.font = UIFont(name: "Bradley Hand", size: 16.0)
        cell.textLabel?.textColor = UIColor.whiteColor()
        cell.textLabel?.text = self.scores![indexPath.row].name
        
        cell.detailTextLabel?.font = UIFont(name: "Bradley Hand", size: 18.0)
        cell.detailTextLabel?.textColor = UIColor.whiteColor()
        cell.detailTextLabel?.text = toString(self.scores![indexPath.row].score)
        
        cell.backgroundColor = UIColor.clearColor()
        cell.separatorInset = UIEdgeInsetsZero
        
        return cell
    }
}
