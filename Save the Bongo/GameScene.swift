//
//  GameScene.swift
//  Save the Bongo
//
//  Created by Maykon Meier on 2/16/15.
//  Copyright (c) 2015 Maykon Meier. All rights reserved.
//

import SpriteKit
import AVFoundation

var backgroundMusicPlayer: AVAudioPlayer!

func playBackgroundMusic(filename: String) {
    let url = NSBundle.mainBundle().URLForResource(filename, withExtension: nil)
    if (url == nil) {
        println("Could not find file: \(filename)")
        return
    }
    
    var error: NSError? = nil
    backgroundMusicPlayer = AVAudioPlayer(contentsOfURL: url, error: &error)
    if backgroundMusicPlayer == nil {
        println("Could not create audio player: \(error!)")
        return
    }
    
    backgroundMusicPlayer.numberOfLoops = -1
    backgroundMusicPlayer.prepareToPlay()
    backgroundMusicPlayer.play()
}

func stopBackgroundMusicPlayer() {
    backgroundMusicPlayer.stop()
}

struct PhysicsCategory {
    static let None         : UInt32 = 0
    static let All          : UInt32 = UInt32.max
    static let Monster      : UInt32 = 0b1
    static let Projectile   : UInt32 = 0b10
}

func + (left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x + right.x, y: left.y + right.y)
}

func - (left: CGPoint, right: CGPoint) -> CGPoint {
    return CGPoint(x: left.x - right.x, y: left.y - right.y)
}

func * (point: CGPoint, scalar: CGFloat) -> CGPoint {
    return CGPoint(x: point.x * scalar, y: point.y * scalar)
}

func / (point: CGPoint, scalar: CGFloat) -> CGPoint {
    return CGPoint(x: point.x / scalar, y: point.y / scalar)
}

#if !(arch(x86_64) || arch(arm64))
    func sqrt(a: CGFloat) -> CGFloat {
    return CGFloat(sqrtf(Float(a)))
    }
#endif

extension CGPoint {
    func length() -> CGFloat {
        return sqrt(x*x + y*y)
    }
    
    func normalized() -> CGPoint {
        return self / length()
    }
}

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    let imageShot = "coconut"
    let imageMonster = "monk"
    let imageBongo = "monkey"
    
    let player = SKSpriteNode(imageNamed: "monkey")
    let killsLabel = SKLabelNode(fontNamed: "Chalkduster")
    let shotsLabel = SKLabelNode(fontNamed: "Chalkduster")
    let scoreLabel = SKLabelNode(fontNamed: "Chalkduster")
    
    let bonusLabel = SKLabelNode(fontNamed: "Chalkduster")
    
    let pauseButton = SKLabelNode(fontNamed: "Chalkduster")
    
    let lostShot = -5
    let killShot = 10
    let minimumScore = 0
    
    let bonus10:Int = 100
    
    var vc:UIViewController?
    
    var monstersDestroyed = 0
    var shotsFired = 0
    var score = 0
    var inARow:Int = 0
    
    let topAreaHeight:CGFloat = 30
    let topSafeAreaWidth:CGFloat = 80
    
    var screenSize:CGSize!
    
    override init(size: CGSize) {
        super.init(size: size)
        self.screenSize = size
        
        let back:SKSpriteNode = SKSpriteNode(imageNamed: "Background")
        back.position = CGPointMake(self.size.width/2, self.size.height/2)
        back.scene?.scaleMode = SKSceneScaleMode.AspectFill
        back.zPosition = -10
        
        addChild(back)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToView(view: SKView) {
        playBackgroundMusic("CheeZeeJungle.caf")
        
//        backgroundColor = SKColor.whiteColor()
        
        player.position = CGPoint(x: size.width * 0.1, y: size.height * 0.5 - 20)
        
        scoreLabel.text = Text.translate("SCORE") + ": 0"
        scoreLabel.name = "kills-button"
        scoreLabel.fontSize = 20
        scoreLabel.fontColor = SKColor.whiteColor()
        scoreLabel.position = CGPoint(x: size.width/2, y: size.height - self.topAreaHeight)
        
        killsLabel.text = Text.translate("KILLS") + ": 0"
        killsLabel.name = "kills-button"
        killsLabel.fontSize = 20
        killsLabel.fontColor = SKColor.whiteColor()
        killsLabel.position = CGPoint(x: size.width - 80, y: size.height - self.topAreaHeight)
        
        shotsLabel.text = Text.translate("SHOTS") + ": 0"
        shotsLabel.fontSize = 20
        shotsLabel.fontColor = SKColor.whiteColor()
        shotsLabel.position = CGPoint(x: 60, y: size.height - 30)
        
        bonusLabel.text = Text.translate("YOU_HAVE_EARNED_BONUS_100")
        bonusLabel.fontSize = 14
        bonusLabel.fontColor = UIColor.whiteColor()
        bonusLabel.alpha = 0
        bonusLabel.zPosition = 50.0
        bonusLabel.position = CGPoint(x: size.width/2, y: size.height - self.topAreaHeight - 40)
        
        addChild(player)
        addChild(killsLabel)
        addChild(shotsLabel)
        addChild(scoreLabel)
        addChild(bonusLabel)
        
        physicsWorld.gravity = CGVectorMake(0, 0)
        physicsWorld.contactDelegate = self
        
        runAction(SKAction.repeatActionForever(
            SKAction.sequence([
                SKAction.waitForDuration(1.0),
                SKAction.runBlock(addMonster)
                ])
            ))
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        var firstBody: SKPhysicsBody
        var secondBody: SKPhysicsBody
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        } else {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        if ((firstBody.categoryBitMask & PhysicsCategory.Monster != 0) && (secondBody.categoryBitMask & PhysicsCategory.Projectile != 0)) {
            projectileDidCollideWithMonster(firstBody.node as SKSpriteNode, monster: secondBody.node as SKSpriteNode)
            self.killsLabel.text = Text.translate("KILLS") + ": \(self.monstersDestroyed)"
            updateScore(self.killShot)
            inARow++
            if (inARow == 10) {
                inARow = 0
                var flashAction:SKAction! = SKAction.sequence([SKAction.fadeInWithDuration(0.2), SKAction.waitForDuration(1), SKAction.fadeOutWithDuration(0.2)])
                
                self.bonusLabel.runAction(flashAction, completion: nil)
                updateScore(self.bonus10)
            }
        }
    }
    
    override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
        let touch = touches.anyObject() as UITouch
        let touchLocation = touch.locationInNode(self)
        let node:SKNode = self.nodeAtPoint(touchLocation)
        /*
        if (touchLocation.y > (self.screenSize.height - self.topAreaHeight)
            && touchLocation.x > (self.screenSize.width - self.topSafeAreaWidth)) {
            println("PAUSE: Do not shot! \(self.paused)")
            (self.vc as GameViewController).pauseGame()
            return
        }
        
        if (self.paused) {
            (self.vc as GameViewController).pauseGame()
        }*/
        
//        if (node.name == "pause-game") {
//            return
//        }
        
        shotsFired++
        runAction(SKAction.playSoundFileNamed("Slice.caf", waitForCompletion: false))
        
        let projectile = SKSpriteNode(imageNamed: self.imageShot)
        projectile.position = player.position
        
        projectile.physicsBody = SKPhysicsBody(circleOfRadius: projectile.size.width/2)
        projectile.physicsBody?.dynamic = true
        projectile.physicsBody?.categoryBitMask = PhysicsCategory.Projectile
        projectile.physicsBody?.contactTestBitMask = PhysicsCategory.Monster
        projectile.physicsBody?.collisionBitMask = PhysicsCategory.None
        projectile.physicsBody?.usesPreciseCollisionDetection = true
        
        let offset = touchLocation - projectile.position
        
        if (offset.x < 0) { return }
        
        addChild(projectile)
        
        self.shotsLabel.text = Text.translate("SHOTS") + ": \(shotsFired)"
        
        let direction = offset.normalized()
        
        let shootAmount = direction * 1000
        
        let realDest = shootAmount + projectile.position
        
        let actionMove = SKAction.moveTo(realDest, duration: 2.0)
        
        let actionMoveDone = SKAction.runBlock() {
            let remove = SKAction.removeFromParent()
            self.updateScore(self.lostShot)
            self.inARow = 0
        }
        projectile.runAction(SKAction.sequence([actionMove, actionMoveDone]))
    }
    
    func projectileDidCollideWithMonster(projectile:SKSpriteNode, monster:SKSpriteNode) {
        projectile.removeFromParent()
        monster.removeFromParent()
        monstersDestroyed++
        /*
        if (monstersDestroyed > 30) {
            let reveal = SKTransition.crossFadeWithDuration(0.5)
            let gameOverScene = GameOverScene(size: self.size, won: true)
            gameOverScene.vc = self.vc
            self.view?.presentScene(gameOverScene, transition: reveal)
        }
        */
    }
    
    func random() -> CGFloat {
        return CGFloat(Float(arc4random()) / 0xFFFFFFFF)
    }
    
    func random(#min: CGFloat, max: CGFloat) -> CGFloat {
        return random() * (max - min) + min
    }
    
    func addMonster() {
        let monster = SKSpriteNode(imageNamed: self.imageMonster)
        monster.physicsBody = SKPhysicsBody(rectangleOfSize: monster.size)
        monster.physicsBody?.dynamic = true
        monster.physicsBody?.categoryBitMask = PhysicsCategory.Monster
        monster.physicsBody?.contactTestBitMask = PhysicsCategory.Projectile
        monster.physicsBody?.collisionBitMask = PhysicsCategory.None
        
        let actualY = random(min: monster.size.height/2, max: size.height - monster.size.height/2 - 50)
        
        monster.position = CGPoint(x: size.width + monster.size.width/2, y: actualY)
        
        addChild(monster)
        
        let actualDuration = random(min: CGFloat(4.0), max: CGFloat(2.0));
        
        let actionMove = SKAction.moveTo(CGPoint(x: -monster.size.width/2, y: actualY), duration: NSTimeInterval(actualDuration))
        
        let actionMoveDone = SKAction.removeFromParent()
        
        let loseAction = SKAction.runBlock() {
            stopBackgroundMusicPlayer()
            let reveal = SKTransition.crossFadeWithDuration(0.5)
            let gameOverScene = GameOverScene(size: self.size, score: self.score)
            gameOverScene.vc = self.vc as GameViewController
            self.view?.presentScene(gameOverScene, transition: reveal)
        }
        monster.runAction(SKAction.sequence([actionMove, loseAction, actionMoveDone]))
    }
    
    func updateScore(score:Int) {
        self.score = self.score + score > self.minimumScore ? self.score + score : 0
        self.scoreLabel.text = Text.translate("SCORE") + ": \(self.score)"
    }
}
