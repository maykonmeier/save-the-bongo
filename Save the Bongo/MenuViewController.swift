//
//  MenuViewController.swift
//  CocoBongo
//
//  Created by Maykon Meier on 2/7/15.
//  Copyright (c) 2015 Maykon Ricardo Meier. All rights reserved.
//

import UIKit
import Crashlytics
import Fabric

class MenuViewController: UIViewController, FBLoginViewDelegate {
    
    @IBOutlet var btPlay: UIButton!
    @IBOutlet var btRanking: UIButton!
    
    @IBOutlet var fbLoginView : FBLoginView!
    @IBOutlet var lWelcome : UILabel!
    @IBOutlet var profileV : FBProfilePictureView!
    
    @IBOutlet var last_score: UILabel!
    @IBOutlet var score: UILabel!
    
    let stringHi:String = NSLocalizedString("HI", tableName: nil, bundle: NSBundle.mainBundle(), value: "HI", comment: "Hi")
    
    let stringGuest:String = NSLocalizedString("GUEST", tableName: nil, bundle: NSBundle.mainBundle(), value: "GUEST", comment: "Guest")
    
    @IBAction func rankingButton(sender: UIButton) {
        let rankingVC:RankingViewController = RankingViewController(nibName: "RankingViewController", bundle: nil)
        rankingVC.scores = UserData.sharedInstance.getScoresStruct()
        self.presentViewController(rankingVC, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configureButtons()
        
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "Home")!)
        
        self.fbLoginView.delegate = self
        self.fbLoginView.readPermissions = ["public_profile", "email", "user_friends", "publish_actions"]
        
        let btWidth:Int = 120
        let btHeight:Int = 40
        
        self.view.bounds.size.width/2
        self.view.bounds.size.height/2
    }
    
    override func viewDidAppear(animated: Bool) {
        setScores(UserData.sharedInstance.score, last_score: UserData.sharedInstance.last_score)
    }
    
    private func configureButtons() {
        self.btPlay.titleLabel?.textAlignment = NSTextAlignment.Center
        self.btRanking.titleLabel?.textAlignment = NSTextAlignment.Center
//        self.btPlay.setImage(UIImage(named: "Button"), forState: .Normal)
        
//        self.btPlay.backgroundColor = UIColor.bl
//        self.btRanking.backgroundColor = UIColor.blackColor()
    }
    
    private func setScores(score:Int, last_score:Int) {
        self.last_score.text = "\(last_score)"
        self.score.text = "\(score)"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Facebook delegate
    func loginViewShowingLoggedInUser(loginView : FBLoginView!) {
        println("User Logged In")
//        UserData.sharedInstance.getScores()
//        UserData.sharedInstance.getFriends()
    }
    
    func loginViewFetchedUserInfo(loginView : FBLoginView!, user: FBGraphUser) {
//        debug(user)
        println("Setting user ID as: \(user.objectID)")
        UserData.sharedInstance.setFacebookID(user.objectID)
        
        self.lWelcome.text = "\(stringHi), " + user.name
        
        Crashlytics.setUserName(user.name)
        Crashlytics.setUserIdentifier(user.objectID)
        
        self.profileV.profileID = user.objectID
        
        // Get List Of Friends
        var friendsRequest : FBRequest = FBRequest.requestForMyFriends()
        friendsRequest.startWithCompletionHandler{(connection:FBRequestConnection!, result:AnyObject!, error:NSError!) -> Void in
            var resultdict = result as NSDictionary
//            println("Result Dict: \(resultdict)")
            var data : NSArray = resultdict.objectForKey("data") as NSArray
            
            for i in 0..<data.count {
                let valueDict : NSDictionary = data[i] as NSDictionary
                let id = valueDict.objectForKey("id") as String
                let name = valueDict.objectForKey("name") as String
                println("the id value is \(name)")
            }
            
            var friends = resultdict.objectForKey("data") as NSArray
            println("Found \(friends.count) friends")
        }
    }
    
    func loginViewShowingLoggedOutUser(loginView : FBLoginView!) {
        self.lWelcome.text = "\(stringHi), \(stringGuest)"
        UserData.sharedInstance.setFacebookID(nil)
        self.profileV.profileID = nil
        println("User Logged Out")
    }
    
    func loginView(loginView : FBLoginView!, handleError:NSError) {
        println("Error: \(handleError.localizedDescription)")
    }
    
    // MARK: - Debug
    
    private func debug(user:FBGraphUser) {
        println("User: \(user)")
        println("User ID: \(user.objectID)")
        println("User Name: \(user.name)")
        var userEmail = user.objectForKey("email") as String
        println("User Email: \(userEmail)")
    }
    
}
