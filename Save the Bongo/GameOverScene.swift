//
//  GameOverScene.swift
//  Save the Bongo
//
//  Created by Maykon Meier on 2/16/15.
//  Copyright (c) 2015 Maykon Meier. All rights reserved.
//

import Foundation
import SpriteKit

class GameOverScene: SKScene {
    
    let restart = SKLabelNode(fontNamed: "AppleSDGothicNeo-Bold")
    let menu = SKLabelNode(fontNamed: "AppleSDGothicNeo-Bold")
    let score = SKLabelNode(fontNamed: "AppleSDGothicNeo-Bold")
    
    var vc:UIViewController?
    
    let idRestartButton = "restart_button"
    let idMenuButton = "menu_button"
    
    init(size: CGSize, score: Int) {
        super.init(size: size)
        
        let back:SKSpriteNode = SKSpriteNode(imageNamed: "Background")
        back.position = CGPointMake(self.frame.size.width/2, self.frame.size.height/2)
        back.scene?.scaleMode = SKSceneScaleMode.AspectFill
        
        addChild(back)
        
        let best = UserData.sharedInstance.isMyBest(score)
        
        var message = best ? Text.translate("YOU_ROCK") : Text.translate("ALMOST_THERE")
        
        let label = SKLabelNode(fontNamed: "Chalkduster")
        label.text = message
        label.fontSize = 40
        label.fontColor = SKColor.blackColor()
        label.position = CGPoint(x: size.width/2, y: size.height/2 + 40)
        addChild(label)
        
        let backRestart:SKSpriteNode = SKSpriteNode(imageNamed: "Button")
        backRestart.zPosition = 12.0
        backRestart.position = CGPoint(x: size.width/2, y: size.height/2 + 5)
        backRestart.zPosition = 20.0
        addChild(backRestart)
        
        restart.name = idRestartButton
        restart.text = Text.translate("PLAY_AGAIN")
        restart.fontSize = 20
        restart.fontColor = SKColor.whiteColor()
        restart.position = CGPoint(x: size.width/2, y: size.height/2)
        restart.zPosition = 50.0
        addChild(restart)
        
        let backMenu:SKSpriteNode = SKSpriteNode(imageNamed: "Button")
        backMenu.zPosition = 12.0
        backMenu.position = CGPoint(x: size.width/2, y: size.height/2 - 45)
        backMenu.zPosition = 20.0
        addChild(backMenu)
        
        println("SIZE :: \(backMenu.size)")
        
        menu.name = idMenuButton
        menu.text = Text.translate("BACK_TO_MENU")
        menu.fontSize = 20
        menu.fontColor = SKColor.whiteColor()
        menu.position = CGPoint(x: size.width/2, y: size.height/2 - 50)
        menu.zPosition = 50.0
        addChild(menu)
        
        let backScore:SKSpriteNode = SKSpriteNode(imageNamed: "Button")
        backScore.zPosition = 12.0
        backScore.position = CGPoint(x: size.width/2, y: size.height/2 - 95)
        backScore.zPosition = 20.0
        addChild(backScore)

        self.score.name = "score-label"
        self.score.text = "\(score)"
        self.score.fontSize = 20
        self.score.fontColor = SKColor.whiteColor()
        self.score.position = CGPoint(x: size.width/2, y: size.height/2 - 100)
        self.score.zPosition = 50.0
        addChild(self.score)
        
        UserData.sharedInstance.setScore(score)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
        let touch:UITouch = touches.anyObject() as UITouch
        let location:CGPoint = touch.locationInNode(self)
        let node:SKNode = self.nodeAtPoint(location)
        
        if (node.name == idRestartButton) {
            runAction(SKAction.sequence([
                SKAction.runBlock() {
                    let reveal = SKTransition.crossFadeWithDuration(0.5)
                    let scene = GameScene(size: self.scene!.size)
                    scene.vc = self.vc as GameViewController
                    self.view?.presentScene(scene, transition:reveal)
                }
                ]))
        } else if node.name == idMenuButton {
            (self.vc as GameViewController).backToMenu()
        }
    }
}
