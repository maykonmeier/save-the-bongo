//
//  Text.swift
//  Save the Bongo
//
//  Created by Maykon Meier on 2/28/15.
//  Copyright (c) 2015 Maykon Meier. All rights reserved.
//

import Foundation

class Text {
    
    class func translate(p:String) -> String {
        return NSLocalizedString(p, tableName: nil, bundle: NSBundle.mainBundle(), value: p, comment: p)
    }
    
}