//
//  RankingTableViewCell.swift
//  Save the Bongo
//
//  Created by Maykon Meier on 2/28/15.
//  Copyright (c) 2015 Maykon Meier. All rights reserved.
//

import UIKit

class RankingTableViewCell: UITableViewCell {
    
    @IBOutlet var nameLabel:UILabel
    @IBOutlet var scoreLabel:UILabel

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
