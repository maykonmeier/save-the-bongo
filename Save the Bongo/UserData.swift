//
//  UserData.swift
//  Save the Bongo
//
//  Created by Maykon Meier on 2/16/15.
//  Copyright (c) 2015 Maykon Meier. All rights reserved.
//

import Crashlytics

protocol RankingDelegate {
    func startUpdatingData()
    func setScores(scores:[Score]?)
    func finishUpdate()
    func error(error:NSError?)
}

class UserData {
    
    let userDefaults:NSUserDefaults! = NSUserDefaults.standardUserDefaults()
    let key_facebook_id:String! = "facebook_id"
    let key_score:String! = "facebook_score"
    let key_last_score:String! = "facebook_last_score"
    let key_friends:String! = "facebook_friends"
    
    var delegateRanking:RankingDelegate?
    
    var facebook_id:String?
    var score:Int! = 0
    var last_score:Int! = 0
    
    var friends: NSDictionary?
    
    var nameArray: NSMutableArray = NSMutableArray()
    var scoreArray: NSMutableArray = NSMutableArray()
    var globalDict: NSMutableDictionary = NSMutableDictionary()
    
    var scores:[Score]? = []
    
    var name: String = NSString()
    
    class var sharedInstance: UserData {
        struct Static {
            static var instance: UserData?
            static var token: dispatch_once_t = 0
        }
        
        dispatch_once(&Static.token) {
            Static.instance = UserData()
        }
        
        return Static.instance!
    }
    
    init() {
        if let fid: AnyObject = self.userDefaults.objectForKey(self.key_facebook_id) {
            self.facebook_id = fid as? String
        }
        if let s: AnyObject = self.userDefaults.objectForKey(self.key_score) {
            self.score = s as? Int
        }
        if let ls: AnyObject = self.userDefaults.objectForKey(self.key_last_score) {
            self.last_score = ls as? Int
        }
        if let friends: AnyObject = self.userDefaults.objectForKey(self.key_friends) {
            self.friends = friends as? NSDictionary
        }
    }
    
    func setFacebookID(id:String!) {
        if id == nil {
            self.userDefaults.removeObjectForKey(self.key_facebook_id)
        } else {
            self.userDefaults.setObject(id, forKey: self.key_facebook_id)
        }
        save()
    }
    
    func getFriends() {
        var friendsRequest : FBRequest = FBRequest.requestForMyFriends()
        friendsRequest.startWithCompletionHandler{(connection:FBRequestConnection!, result:AnyObject!, error:NSError!) -> Void in
            var resultdict = result as NSDictionary
//            println("Result Dict: \(resultdict)")
            var data : NSArray = resultdict.objectForKey("data") as NSArray
            
            for i in 0..<data.count {
                let valueDict : NSDictionary = data[i] as NSDictionary
                let id = valueDict.objectForKey("id") as String
                println("the id value is \(id)")
            }
            
            var friends = resultdict.objectForKey("data") as NSArray
//            println("Found \(friends.count) friends")
        }
        
        FBRequestConnection.startWithGraphPath("/app/scores", parameters: nil, HTTPMethod: "GET", completionHandler: {(connection: FBRequestConnection!, result: AnyObject!, error: NSError!) -> Void in
            var resultdict = result as NSDictionary!
            let userDefaults = NSUserDefaults.standardUserDefaults()
            var data: NSArray! = resultdict.objectForKey("data") as NSArray!
            var int = data.count as Int
//            println("Dicte: \(resultdict)")
            //println("Int: \(int)")
            for (var i = 0; i < int; i++) {
                let packet: NSMutableDictionary = data.objectAtIndex(i) as NSMutableDictionary
                let score = packet.objectForKey("score") as Int
                var scoreStr = String(score)
                self.scoreArray.addObject(score)
                let user: NSMutableDictionary = packet.objectForKey("user") as NSMutableDictionary
                let name = user.objectForKey("name") as String
                if (self.name == name) {
                    let highScore = score
                    userDefaults.setValue(highScore, forKey: "fbHighScore")
                }
                self.nameArray.addObject(name)
                self.globalDict.setValue(scoreStr, forKey: name)
//                self.scores?.append(Score(name: name, score: score))
            }
            userDefaults.setValue(self.nameArray, forKey: "nameArray")
            userDefaults.setValue(self.scoreArray, forKey: "scoreArray")
            userDefaults.setValue(self.globalDict, forKey: "dict")
//            println("Scores: \(self.globalDict)")
        })
        
        /*
        FBRequestConnection.startWithGraphPath("/me/friends", parameters: nil, HTTPMethod: "GET", completionHandler: {(connect: FBRequestConnection!, result: AnyObject!, error: NSError!) -> Void in
            
            var dict = result as NSDictionary
//            self.friends = dict.objectForKey("data") as NSDictionary
            var d = dict.objectForKey("data") as NSArray
            println("Dict: \(d)")
            self.save()
        })

*/
    }
    
    func callDelegate() {
        self.delegateRanking?.setScores(self.getScoresStruct())
        self.delegateRanking?.finishUpdate()
    }
    
    func getScoresStruct() -> [Score]? {
        return self.scores
    }
    
    func getScores() {
        
        if self.facebook_id == nil {
            
        }
        
        // FLAVIO 1046552682028822
        // SUZI 912315925445279
        
        self.delegateRanking?.startUpdatingData()
        
        FBRequestConnection.startWithGraphPath("/app/scores", parameters: nil, HTTPMethod: "GET", completionHandler: {(connection: FBRequestConnection!, result: AnyObject!, error: NSError!) -> Void in
            if let e = error {
                self.delegateRanking?.error(e)
                return
            }
            self.scores?.removeAll(keepCapacity: true)
            var resultdict = result as NSDictionary!
            let userDefaults = NSUserDefaults.standardUserDefaults()
            var data: NSArray! = resultdict.objectForKey("data") as NSArray!
            var int = data.count as Int
//            println("getScores dict: \(resultdict)")
            for (var i = 0; i < int; i++) {
                let packet: NSMutableDictionary = data.objectAtIndex(i) as NSMutableDictionary
                let score = packet.objectForKey("score") as Int
                var scoreStr = String(score)
                self.scoreArray.addObject(score)
                let user: NSMutableDictionary = packet.objectForKey("user") as NSMutableDictionary
                let name = user.objectForKey("name") as String
                if (self.name == name) {
                    let highScore = score
                    userDefaults.setValue(highScore, forKey: "fbHighScore")
                }
                self.scores?.append(Score(name: name, score: score))
                self.nameArray.addObject(name)
                self.globalDict.setValue(scoreStr, forKey: name)
            }
            userDefaults.setValue(self.nameArray, forKey: "nameArray")
            userDefaults.setValue(self.scoreArray, forKey: "scoreArray")
            userDefaults.setValue(self.globalDict, forKey: "dict")
            self.callDelegate()
//            println("Scores: \(self.scoreArray)")
//            println("Scores: \(self.nameArray)")
        })
    }
    
    private func setFaceScore(score:Int!) {
        if (self.facebook_id? == nil) {
            println("Trying to set score but facebook_id is nil")
            return;
        }
        var param:[String:Int] = ["score":score]
        
        FBRequestConnection.startWithGraphPath("/\(self.facebook_id!)/scores", parameters: param, HTTPMethod: "POST", completionHandler: {(connect: FBRequestConnection!, result: AnyObject!, error: NSError!) -> Void in
            
            if let e = error {
                println("ERROR: \(e)")
                return
            }
            
            var dict = result as NSDictionary
            //            self.friends = dict.objectForKey("data") as NSDictionary
//            var d = dict.objectForKey("data") as NSArray
            println("Dict Save Score: \(dict)")
//            self.save()
        })
    }
    
    func isMyBest(toTest:Int!) -> Bool {
        return toTest > self.score
    }
    
    func setScore(score:Int!) {
        if isMyBest(score) {
            self.score = score
            self.setFaceScore(score)
            self.userDefaults.setObject(score, forKey: self.key_score)
        }
        self.last_score = score
        self.userDefaults.setObject(score, forKey: self.key_last_score)
    }
    
    private func save() -> Bool {
        return userDefaults.synchronize()
    }
    
}