
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// Bolts
#define COCOAPODS_POD_AVAILABLE_Bolts
#define COCOAPODS_VERSION_MAJOR_Bolts 1
#define COCOAPODS_VERSION_MINOR_Bolts 1
#define COCOAPODS_VERSION_PATCH_Bolts 3

// Fabric/Core
#define COCOAPODS_POD_AVAILABLE_Fabric_Core
#define COCOAPODS_VERSION_MAJOR_Fabric_Core 1
#define COCOAPODS_VERSION_MINOR_Fabric_Core 1
#define COCOAPODS_VERSION_PATCH_Fabric_Core 2

// Fabric/Crashlytics
#define COCOAPODS_POD_AVAILABLE_Fabric_Crashlytics
#define COCOAPODS_VERSION_MAJOR_Fabric_Crashlytics 1
#define COCOAPODS_VERSION_MINOR_Fabric_Crashlytics 1
#define COCOAPODS_VERSION_PATCH_Fabric_Crashlytics 2

// Facebook-iOS-SDK
#define COCOAPODS_POD_AVAILABLE_Facebook_iOS_SDK
#define COCOAPODS_VERSION_MAJOR_Facebook_iOS_SDK 3
#define COCOAPODS_VERSION_MINOR_Facebook_iOS_SDK 23
#define COCOAPODS_VERSION_PATCH_Facebook_iOS_SDK 0

